package Hibernate;


import org.hibernate.Session;

import java.time.LocalDate;


public class App 
{
    public static void main( String[] args ){

        Session session = HibernateUtil.getSessionFactory().openSession();

        session.beginTransaction();

        Employee employee;
        LocalDate date;
        date = LocalDate.of(2000,01,01);



        // Check database version

        String sql = "SELECT VERSION()";
        String result = (String) session.createNativeQuery(sql).getSingleResult();
        System.out.println(result);
        session.getTransaction().commit();
        session.close();
        HibernateUtil.shutdown();
    }
}
