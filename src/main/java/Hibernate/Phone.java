package Hibernate;

import javax.persistence.*;

@Entity
@Table(name = "phone")
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    int id;

    @Column
    String model;

    @Column
    String mark;

    @OneToOne(mappedBy = "phone",
            fetch = FetchType.EAGER)
    Employee phoneOwner;

    public Phone() {
    }

}
