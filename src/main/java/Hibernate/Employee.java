package Hibernate;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    int id;

    @Column
    String imie;

    @Column
    String nazwisko;

    @Column(name = "data_urodzenia")
    LocalDate dataUrodzenia;

    @Column
    String eEmail;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="userPhone_id")
    Phone phone;

    @OneToMany(mappedBy = "taskEmployee")
    List<Task> tasks = new LinkedList<>();

    @ManyToMany
    @JoinTable(
            name="project_employee",
            joinColumns = {
                    @JoinColumn(name="project_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "employee_id")}
    )
    Set<Project> projects = new HashSet<>();

    public Employee(String imie, String nazwisko, LocalDate dataUrodzenia, String eEmail) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        this.eEmail = eEmail;
    }

    public Employee() {
    }

    @Override
    public String toString() {
        return "Employee{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", dataUrodzenia=" + dataUrodzenia +
                ", eEmail='" + eEmail + '\'' +
                '}';
    }


}
