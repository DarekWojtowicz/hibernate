package Hibernate;

import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.List;

public class EmployeeApp {
    public static void main(String[] args) {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        final EntityManager em = sessionFactory.createEntityManager();
        Employee employee = new Employee("Anna", "Kowalska", LocalDate.of(2000,01,01),"email@email.com.pl" );
        Phone phone = new Phone();
        phone.mark = "LG";
        phone.model = "V30";
        employee.phone = phone;

        Employee employee2 = new Employee("Adam", "Nowak", LocalDate.of(1999,02,02),"email2@mems.pl");
        Phone phone2 = new Phone();
        phone2.mark = "Samsung";
        phone2.model = "S10";
        Task task1 = new Task();
        task1.title = "zadanie 1";
        task1.content = "wykopać rów 300m";
        task1.taskType = TaskType.HIGH_PRIORITY;
        task1.taskEmployee = employee;
        Task task2 = new Task();
        task2.title = "zadanie 2";
        task2.content = "wykopać rów 100m";
        task2.taskType = TaskType.HIGH_PRIORITY;
        task2.taskEmployee = employee;
        employee.tasks.add(task1);
        employee.tasks.add(task2);

        Project project = new Project();
        project.title = "Java";
        employee.projects.add(project);

        employee2.phone = phone2;
        em.getTransaction().begin();
        em.persist(employee);
        em.persist(phone);
        em.persist(task1);
        em.persist(task2);
        em.persist(project);
        em.persist(employee2);
        em.persist(phone2);

        Query query = em.createQuery("from Employee AS e JOIN fetch e.phone AS p WHERE p.mark =:mark");
        query.setParameter("mark", "LG");
        List<Employee> employeeList = query.getResultList();
        employeeList.forEach(x-> System.out.println(x.imie + " " + x.nazwisko));

        em.getTransaction().commit();
        HibernateUtil.shutdown();
    }
}



