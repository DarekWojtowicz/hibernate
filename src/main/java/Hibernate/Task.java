package Hibernate;

import javax.persistence.*;

@Entity
@Table(name = "task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    int taskID;

    @Column
    String title;

    @Column
    String content;

    @Column
    @Enumerated(EnumType.STRING)
    TaskType taskType;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    Employee taskEmployee;

    public Task() {
    }

}
