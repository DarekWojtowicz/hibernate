package Hibernate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    int projectID;

    @Column
    String title;

    @ManyToMany(mappedBy = "projects")
    Set<Employee> projectEmployee =new HashSet<>();


}
